package com.evgeny_alex.automated_system.timer;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.swing.text.html.HTML;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс SystemTimer. Каждый сутки заполняет старую Map oldMap
 * значениями из текущей Map currentMap
 */
@Component
@AllArgsConstructor
@EnableScheduling
public class SystemTimer {

    private static final Logger log = Logger.getLogger(SystemTimer.class);

    private Map<URL, HTML> oldMap;

    private Map<URL, HTML> currentMap;

    /**
     * Метод заполняет <b>oldMap</b> значениями из <b>currentMap</b>.
     * fixedRate - интервал заполнения (каждые сутки).
     */
    @Scheduled(fixedRate = 24*60*60*1000)
    public void fillOldMap() {

        oldMap = new HashMap<>();

        oldMap.putAll(currentMap);

        log.info("Changed hash table.");
    }
}
