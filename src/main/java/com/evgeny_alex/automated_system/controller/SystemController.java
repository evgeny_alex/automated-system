package com.evgeny_alex.automated_system.controller;

import com.evgeny_alex.automated_system.service.SystemService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URL;

/**
 * Класс-контроллер, который обрабатывает запросы
 * связанные с работой системы.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RestController
@AllArgsConstructor
public class SystemController {

    private SystemService systemService;

    /**
     * Метод обрабатывает Post запрос по добавлению новой страницы.
     *
     * @param url      - название URL
     * @param document - HTML страница
     */
    @PostMapping(value = "/system")
    public void addPage(@RequestParam("url") URL url, @Valid @RequestBody String document) {
        systemService.add(url, document);
    }

    /**
     * Метод обрабатывает Delete запрос по удалению страницы.
     *
     * @param url - название URL
     */
    @DeleteMapping(value = "/system")
    public void deletePage(@RequestParam("url") URL url) {
        systemService.delete(url);
    }

    /**
     * Метод обрабатывает Put запрос по изменению существующей страницы.
     *
     * @param url      - название URL
     * @param document - HTML страница
     */
    @PutMapping(value = "/system")
    public void changePage(@RequestParam("url") URL url, @Valid @RequestBody String document) {
        systemService.change(url, document);
    }
}
