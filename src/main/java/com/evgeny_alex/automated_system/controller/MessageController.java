package com.evgeny_alex.automated_system.controller;

import com.evgeny_alex.automated_system.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Класс-контроллер, который обрабатывает запрос
 * по отправке письма с отчетом.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RestController
@AllArgsConstructor
public class MessageController {

    private MessageService messageService;

    /**
     * Метод обрабатывает Post запрос, для отправки отчета на email,
     * указанный в {@link com.evgeny_alex.automated_system.config.MailConfig}.
     *
     * @param name - имя секретаря, которому отправляется сообщение
     * @return String c отчетным письмом
     */
    @PostMapping(value = "/message")
    public String sendMessage(@RequestParam(name = "name") String name) {
        return messageService.send(name);
    }
}
