package com.evgeny_alex.automated_system;

import com.evgeny_alex.automated_system.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;

/**
 * Главный класс приложения. Содержит метод main с которого начинается программа.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
public class Application {

    /**
     * Метод main, начало программы.
     *
     * @param args - параметры командной строки
     */
    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
