package com.evgeny_alex.automated_system.service;

import com.evgeny_alex.automated_system.config.MailConfig;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Класс-сервис, который отправляет письмо с отчетом.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Service
@AllArgsConstructor
public class MessageService {

    private static final Logger log = Logger.getLogger(MessageService.class);

    /**
     * Map для хранения старой таблицы URL. Spring создает бин и внедряет его.
     */
    private Map<URL, Document> oldMap;

    /**
     * Map для хранения текущей таблицы URL. Так же внедряется бин, созданный Spring.
     */
    private Map<URL, Document> currentMap;

    /**
     * Поле JavaMailSender, для отправки сообщения.
     */
    private JavaMailSender mailSender;

    /**
     * Метод создает объект SimpleMailMessage, устанавливает текст сообщения,
     * тему и получателя.
     *
     * @param name - имя секретаря, которую отправляется сообщение
     * @return текст отчета
     */
    public String send(String name) {
        SimpleMailMessage message = new SimpleMailMessage();

        String strMissingURLs = getMissingURLs().toString();
        String strNewURLs = getNewURLs().toString();
        String strChangedURLs = getChangedURLs().toString();

        String textMessage = getTextMessage(name, strMissingURLs, strNewURLs, strChangedURLs);

        message.setTo(MailConfig.TO_EMAIL);
        message.setSubject("Отчет за последние сутки");
        message.setText(textMessage);

        mailSender.send(message);

        log.info("An email with a report was sent.");

        return textMessage;
    }

    /**
     * Метод с помощью StringBuffer собирает сообщение.
     *
     * @param name           - имя секретаря
     * @param strMissingURLs - строка, со списком исчезнувших URL
     * @param strNewURLs     - строка, со списком новых URL
     * @param strChangedURLs - строка, со списком измененных URL
     * @return - полный текст сообщения
     */
    private String getTextMessage(String name, String strMissingURLs, String strNewURLs, String strChangedURLs) {

        StringBuffer textMessage = new StringBuffer();

        textMessage.append("Здравствуйте, дорогая(ой) ");
        textMessage.append(name);
        textMessage.append(", \nЗа последние сутки во вверенных Вам сайтах произошли следующие изменения:\n");
        textMessage.append("Исчезли следующие страницы: ");
        textMessage.append(strMissingURLs);
        textMessage.append("\nПоявились следующие новые страницы: ");
        textMessage.append(strNewURLs);
        textMessage.append("\nИзменились следующие страницы: ");
        textMessage.append(strChangedURLs);
        textMessage.append("\nС уважением, \nавтоматизированная система мониторинга.");

        return new String(textMessage);
    }

    /**
     * Метод возвращает список потерянных URL.
     *
     * @return - список потерянных URL
     */
    private List<URL> getMissingURLs() {
        return getDifferentUrls(oldMap, currentMap);
    }

    /**
     * Метод возвращает список новых URL.
     *
     * @return - список новых URL
     */
    private List<URL> getNewURLs() {
        return getDifferentUrls(currentMap, oldMap);
    }

    /**
     * Метод сравнивает mainMap и diffMap. Возвращает список URL, которые есть в mainMap
     * и нет в diffMap.
     *
     * @param mainMap - Map, относительно которой происходит сравнение
     * @param diffMap - Map, которая сравнивается с mainMap (отличается от нее)
     * @return список URL
     */
    private List<URL> getDifferentUrls(Map<URL, Document> mainMap, Map<URL, Document> diffMap) {

        List<URL> list = new ArrayList<>();

        for (Map.Entry<URL, Document> e : mainMap.entrySet()) {

            if (!diffMap.containsKey(e.getKey())) {
                list.add(e.getKey());
            }

        }
        return list;
    }

    /**
     * Метод возвращает список URL, HTML страницы которых были изменены.
     *
     * @return список URL
     */
    private List<URL> getChangedURLs() {

        List<URL> list = new ArrayList<>();

        for (Map.Entry<URL, Document> e : oldMap.entrySet()) {

            if (currentMap.containsKey(e.getKey())) {

                if (!e.getValue().equals(currentMap.get(e.getKey()))) {
                    list.add(e.getKey());
                }

            }

        }

        return list;
    }
}
