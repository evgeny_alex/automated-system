package com.evgeny_alex.automated_system.service;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.Map;

@Service
@AllArgsConstructor
public class SystemService {

    private static final Logger log = Logger.getLogger(SystemService.class);

    /**
     * Map для хранения текущей таблицы URL. Так же внедряется бин, созданный Spring.
     */
    private Map<URL, Document> currentMap;

    /**
     * Метод добавляет в таблицу новый URL и HTML.
     *
     * @param url         - название URL
     * @param strDocument - строка, содержащая HTML (преобразуется в Document с помощью Jsoap)
     */
    public synchronized void add(URL url, String strDocument) {

        Document document = Jsoup.parse(strDocument);

        currentMap.put(url, document);

        log.info("Added page: " + url + ".");
    }

    /**
     * Метод удаляет URL из Map
     *
     * @param url - название удаляемого URL
     */
    public synchronized void delete(URL url) {

        currentMap.remove(url);

        log.info("Removed page: " + url + ".");
    }

    /**
     * Метод изменяет HTML по указанному URL.
     *
     * @param url - название URL
     * @param strDocument - строка, содержащая новый HTML (преобразуется в Document с помощью Jsoap)
     */
    public synchronized void change(URL url, String strDocument) {

        Document document = Jsoup.parse(strDocument);

        currentMap.replace(url, document);

        log.info("Changed page: " + url + ".");
    }
}
