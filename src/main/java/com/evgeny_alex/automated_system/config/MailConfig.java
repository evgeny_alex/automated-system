package com.evgeny_alex.automated_system.config;

/**
 * Класс содержит константы для отправки сообщений.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
public class MailConfig {

    public static final String FROM_EMAIL = "from.mail.test.java@gmail.com";

    public static final String FROM_PASSWORD = "Qwerty123qwe";

    public static final String TO_EMAIL = "e.aleksandrov1@g.nsu.ru";
}
