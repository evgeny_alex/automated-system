package com.evgeny_alex.automated_system.config;

import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.net.URL;
import java.util.HashMap;
import java.util.Properties;

/**
 * Класс конфигурации Spring Boot приложения.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.evgeny_alex.automated_system.timer", "com.evgeny_alex.automated_system.controller", "com.evgeny_alex.automated_system.service"})
public class ApplicationConfig {

    /**
     * Метод создает бин HashMap для хранения старой таблицы.
     *
     * @return HashMap<URL, Document>
     */
    @Bean
    public HashMap<URL, Document> oldMap() {
        return initMap();
    }

    /**
     * Метод создает бин HashMap для хранения текущей таблицы.
     * Старую и текущую в начале инициализируем идентичными map.
     *
     * @return HashMap<URL, Document>
     */
    @Bean
    public HashMap<URL, Document> currentMap() {
        return initMap();
    }

    /**
     * Метод создает объект JavaMailSender, с помощью
     * которого будем отправлять сообщение с отчетом.
     *
     * @return JavaMailSender
     */
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername(MailConfig.FROM_EMAIL);
        mailSender.setPassword(MailConfig.FROM_PASSWORD);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    /**
     * Метод инициализирует начальное состояние множества сайтов,
     * за которыми система ведет отчет.
     *
     * @return HashMap<URL, Document>
     */
    @SneakyThrows
    private HashMap<URL, Document> initMap() {
        HashMap<URL, Document> map = new HashMap<>();

        URL url1 = new URL("https://url1.com");
        URL url2 = new URL("https://url2.com");
        URL url3 = new URL("https://url3.com");

        Document document1 = Jsoup.parse("<html><head><title>Simple Page</title></head>"
                + "<body>Hello from url 1</body></html>");
        Document document2 = Jsoup.parse("<html><head><title>Simple Page</title></head>"
                + "<body>Hello from url 2</body></html>");
        Document document3 = Jsoup.parse("<html><head><title>Simple Page</title></head>"
                + "<body>Hello from url 3</body></html>");

        map.put(url1, document1);
        map.put(url2, document2);
        map.put(url3, document3);

        return map;
    }
}
