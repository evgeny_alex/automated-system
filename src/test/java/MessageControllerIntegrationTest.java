import com.evgeny_alex.automated_system.config.ApplicationConfig;
import com.evgeny_alex.automated_system.controller.MessageController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Интеграционные тесты MessageController.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@WebMvcTest(MessageController.class)
public class MessageControllerIntegrationTest {

    /**
     * Поле mock, для тестирования HTTP-запросов
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     *  Тест эмитирует работу с системой и запрашивает отчетное письмо.
     */
    @Test
    public void sendMessageTest() throws Exception {
        mockMvc.perform(post("/system?url=https://url4.com")
                .contentType(MediaType.TEXT_PLAIN)
                .content("<html><head><title>Simple Page</title></head>"
                        + "<body>Hello from url 4</body></html>"))
                .andExpect(status().isOk());

        mockMvc.perform(delete("/system?url=https://url3.com"))
                .andExpect(status().isOk());

        mockMvc.perform(put("/system?url=https://url2.com")
                .contentType(MediaType.TEXT_PLAIN)
                .content("<html><head><title>Simple Page</title></head>"
                        + "<body>Hello from url 2.1</body></html>"))
                .andExpect(status().isOk());

        mockMvc.perform(post("http://localhost:8080/message?name=Jack"))
                .andExpect(content().string("Здравствуйте, дорогая(ой) Jack, \n" +
                        "За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n" +
                        "Исчезли следующие страницы: [https://url3.com]\n" +
                        "Появились следующие новые страницы: [https://url4.com]\n" +
                        "Изменились следующие страницы: [https://url2.com]\n" +
                        "С уважением, \n" +
                        "автоматизированная система мониторинга."));
    }

}
