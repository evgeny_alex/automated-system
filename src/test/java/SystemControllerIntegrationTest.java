import com.evgeny_alex.automated_system.config.ApplicationConfig;
import com.evgeny_alex.automated_system.controller.SystemController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Интеграционные тесты SystemController.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@WebMvcTest(SystemController.class)
public class SystemControllerIntegrationTest {

    /**
     * Поле mock, для тестирования HTTP-запросов
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Тест на добавление URL cо строкой HTML.
     */
    @Test
    public void addPageTest() throws Exception {
        mockMvc.perform(post("/system?url=https://url4.com")
                .contentType(MediaType.TEXT_PLAIN)
                .content("<html><head><title>Simple Page</title></head>"
                        + "<body>Hello from url 4</body></html>"))
                .andExpect(status().isOk());
    }

    /**
     * Тест на удаление URL.
     */
    @Test
    public void deletePageTest() throws Exception {
        mockMvc.perform(delete("/system?url=https://url1.com"))
                .andExpect(status().isOk());
    }

    /**
     * Тест на изменение URL cо строкой HTML.
     */
    @Test
    public void changePageTest() throws Exception {
        mockMvc.perform(put("/system?url=https://url2.com")
                .contentType(MediaType.TEXT_PLAIN)
                .content("<html><head><title>Simple Page</title></head>"
                        + "<body>Hello from url 2.1</body></html>"))
                .andExpect(status().isOk());
    }

}
